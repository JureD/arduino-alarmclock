#define rotaryEncoder1 2
#define rotaryEncoder2 8

int counter = 0;
int previousCounter;

void setup(){
  pinMode(rotaryEncoder1, INPUT);
  pinMode(rotaryEncoder2, INPUT);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(rotaryEncoder1), detection, CHANGE);
}

void loop() {
  if (previousCounter != counter) {
  Serial.println(counter);
  previousCounter = counter;
  }
}

void detection() {
  delay(10);
  if (digitalRead(rotaryEncoder1) == digitalRead(rotaryEncoder2)) {
    counter ++;
  } else {
    counter --;
  }
}
