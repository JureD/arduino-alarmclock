#define rotaryEncoder1 2
#define rotaryEncoder2 8

int counter = 0;
int previousCounter;

void setup(){
  pinMode(rotaryEncoder1, INPUT);
  pinMode(rotaryEncoder2, INPUT);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(rotaryEncoder1), detection, CHANGE);
}

void loop() {
  }

void detection() {
  if (digitalRead(rotaryEncoder1) == digitalRead(rotaryEncoder2)) {
    Serial.println("equal");
  } else {
    Serial.println("not");
  }
}
