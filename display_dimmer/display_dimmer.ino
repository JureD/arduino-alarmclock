#define lightSensor A0
#define backlightLED 3

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println(analogRead(lightSensor));
  analogWrite(backlightLED, map(analogRead(lightSensor), 50, 350, 0, 255));
  delay(1000);

}
