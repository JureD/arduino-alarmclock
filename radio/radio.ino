#include <Wire.h>

unsigned char frequencyH = 0;
unsigned char frequencyL = 0;

void setup() {
  Wire.begin();

  Wire.beginTransmission(B1100000);
  Wire.write(B00101010);
  Wire.write(B01001111);
  Wire.write(B10110000);
  Wire.write(0x10);
  Wire.write((byte)0x00);
  Wire.endTransmission();


  
  //set_fequency(88.5);
}

void loop() {
  
}

void set_fequency(float frequency) {
  int new_frequency = (frequency * 1000000 + 225000) / 8192;
  frequencyH = new_frequency >> 8;
  frequencyL = new_frequency & 0Xff;
  Wire.beginTransmission(B1100000);
  Wire.write(frequencyH);
  Wire.write(frequencyL);
  Wire.write(B10110000);
  Wire.write(0x10);
  Wire.write((byte)0x00);
  Wire.endTransmission();
}
