#include <LiquidCrystal.h>


int hour = 0;
String stringHour;
int minute = 0;
String stringMinute;
int second = 0;
String stringSecond;

String stringTime;

const int rs = 12, en = 11, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);


void setup() {
  cli(); //stop interrupts

  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B
  TCNT1  = 0; //initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 15625;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei(); //allow interrupts

  lcd.begin(16, 2);
  lcd.clear();

}

void loop() {
  if (second < 10){
    stringSecond = "0" + String(second);
  } else {
    stringSecond = String(second);
  }
  if (minute < 10){
    stringMinute = "0" + String(minute);
  } else {
    stringMinute = String(minute);
  }
  if (hour < 10){
    stringHour = "0" + String(hour);
  } else {
    stringHour = String(hour);
  }
  stringTime = stringHour + ":" + stringMinute + ":" + stringSecond;
  lcd.setCursor(0, 0);
  lcd.print(stringTime);


}

ISR(TIMER1_COMPA_vect){//timer1 interrupt 1Hz toggles pin 13 (LED)
//generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)
  second += 1;
  if (second == 60){
    minute += 1;
    second = 0;
  } if (minute == 60) {
    hour += 1;
    minute = 0;
  } if (hour == 24) {
    hour = 0;
  }
}
