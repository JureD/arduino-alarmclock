#include <LiquidCrystal.h>

int alarmHour = 0;
String stringAlarmHour;
int alarmMinute = 0;
String stringAlarmMinute;

int alarmON = 0;

String stringAlarm;

const int rs = 12, en = 11, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  lcd.begin(16, 2);
  lcd.clear();
}

void loop() {
  displayAlarm(12, 5, alarmON);

}


void displayAlarm(int alarmHour, int alarmMinute, int alarmON) {
  if (alarmMinute < 10) {
    stringAlarmMinute = "0" + String(alarmMinute);
  } else {
    stringAlarmMinute = String(alarmMinute);
  }
  if (alarmHour < 10) {
    stringAlarmHour = "0" + String(alarmHour);
  } else {
    stringAlarmHour = String(alarmHour);
  }
  if (alarmON) {
    stringAlarm = "*" + stringAlarmHour + ":" + stringAlarmMinute;
  } else {
    stringAlarm = " " + stringAlarmHour + ":" + stringAlarmMinute;
  }
  lcd.setCursor(10, 0);
  lcd.print(stringAlarm);
}
