#include <LiquidCrystal.h>


// Definicija spremeljivk za uro
int hour = 0;
String stringHour;
int minute = 0;
String stringMinute;
int second = 0;
String stringSecond;
String stringTime;

// Definicija spremeljivk za alarm
int alarmHour = 0;
String stringAlarmHour;
int alarmMinute = 0;
String stringAlarmMinute;
int alarmON = 1;
String stringAlarm;

// Definicija spremeljivke za LED backlight
#define backlightLED 3
#define lightSensor A0
int backgroundBrightness = 50;

// Definicija spremenljivk za rotay encoder
#define rotaryEncoder1 2
#define rotaryEncoder2 8
float radioFrequency = 90.5;

// Definicija spremeljivk za LCD
const int rs = 12, en = 11, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  cli(); // Izklopi interrupst

  TCCR1A = 0; // Nastavi celotni TCCR1A register na 0
  TCCR1B = 0; // Nastavi celotni TCCR1B register na 0
  TCNT1  = 0; // Nastavi counter na 0
  OCR1A = 15624; // Nastavi compare match register za 1Hz interrupte
  TCCR1B |= (1 << WGM12); // Vklopi CTC mode
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10); // Nastavi bita CS12 in CS10 na 1 za 1024 prescaler
  TIMSK1 |= (1 << OCIE1A); // Omogoči timer compare interrupts

  sei(); // Dovoli interrupts

  // Definicija pinov za rotary encoder
  pinMode(rotaryEncoder1, INPUT);
  pinMode(rotaryEncoder2, INPUT);
  attachInterrupt(digitalPinToInterrupt(rotaryEncoder1), rotaryEncoder, CHANGE); // Definicija interrupta za rotary encoder

  lcd.begin(16, 2);
  lcd.clear();

}

void loop() {
  displayClock(hour, minute, second);
  displayAlarm(20, 5, alarmON);
  displayRadioFrequency(radioFrequency);
}

// Definicija funkcije, ki se izvede ob counter interruptu
ISR(TIMER1_COMPA_vect) {
  second += 1;
  if (second == 60){
    minute += 1;
    second = 0;
  } if (minute == 60) {
    hour += 1;
    minute = 0;
  } if (hour == 24) {
    hour = 0;
  }
  setBackLight();
}

// Definicija funkcije za upravljanje z rotary encoderjem
void rotaryEncoder() {
  delay(5);
  if (digitalRead(rotaryEncoder1) == digitalRead(rotaryEncoder2)) {
    radioFrequency += 0.1;
  } else {
    radioFrequency -= 0.1;
  }
}

// Definicija funkcije za prikaz ure
void displayClock(int hour, int minute, int second) {
  if (second < 10){
    stringSecond = "0" + String(second);
  } else {
    stringSecond = String(second);
  }
  if (minute < 10){
    stringMinute = "0" + String(minute);
  } else {
    stringMinute = String(minute);
  }
  if (hour < 10){
    stringHour = "0" + String(hour);
  } else {
    stringHour = String(hour);
  }
  stringTime = stringHour + ":" + stringMinute + ":" + stringSecond;
  lcd.setCursor(0, 0);
  lcd.print(stringTime);
}

// Definicija funkcije za prikaz alarma
void displayAlarm(int alarmHour, int alarmMinute, int alarmON) {
  if (alarmMinute < 10) {
    stringAlarmMinute = "0" + String(alarmMinute);
  } else {
    stringAlarmMinute = String(alarmMinute);
  }
  if (alarmHour < 10) {
    stringAlarmHour = "0" + String(alarmHour);
  } else {
    stringAlarmHour = String(alarmHour);
  }
  if (alarmON) {
    stringAlarm = "*" + stringAlarmHour + ":" + stringAlarmMinute;
  } else {
    stringAlarm = " " + stringAlarmHour + ":" + stringAlarmMinute;
  }
  lcd.setCursor(10, 0);
  lcd.print(stringAlarm);
}

// Funkcija za prikaz radijske frekvence
void displayRadioFrequency(float radioFrequency) {
  lcd.setCursor(0, 1);
  lcd.print(String(radioFrequency) + "MHz ");
}

// Definicija funkcije za nastavljanje backlight
void setBackLight() {
  analogWrite(backlightLED, map(analogRead(lightSensor), 50, 350, 0, 255));
}
